package graphQL.models;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "courses")
public class Course {
    private String courseName;
    private String programLeader;
    private String universityID;
    private List<Module> modules;
    private List<String> staffIds;
}
