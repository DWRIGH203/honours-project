package graphQL.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class University {

    private List<Staff> staff;
    private List<Student> students;
    private List<Course> courses;
    private List<Society> societies;
}
