package graphQL.models;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "students")
public class Student {

    private ObjectId id;
    private String forename;
    private String surname;
    private String studentId;
    private String universityID;
    private String email;
    private int level;
    private Map<String, Double> examResults;
    private String courseName;
}
