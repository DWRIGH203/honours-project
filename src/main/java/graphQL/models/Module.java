package graphQL.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Module {
    private String title;
    private String description;
    private int level;
}
