package graphQL.repositories;

import graphQL.models.Course;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CourseRepository extends PagingAndSortingRepository<Course, ObjectId> {

    @Query("{'universityID': ?0 }")
    List<Course> findCoursesByUniversityId(String universityId);
}
