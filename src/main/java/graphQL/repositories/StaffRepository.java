package graphQL.repositories;

import graphQL.models.Staff;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface StaffRepository extends PagingAndSortingRepository<Staff, ObjectId> {


    @Query("{'universityID': ?0 }")
    List<Staff> findStaffByUniversityId(String universityId);
}
