package graphQL.repositories;

import graphQL.models.Staff;
import graphQL.models.Student;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface StudentRepository extends PagingAndSortingRepository<Student, ObjectId> {

    @Query("{'universityID': ?0 }")
    List<Student> findStudentsByUniversityId(String universityID);
}
