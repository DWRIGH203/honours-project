package graphQL.repositories;

import graphQL.models.Society;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SocietyRepository extends PagingAndSortingRepository<Society, ObjectId> {

    @Query("{'universityID': ?0 }")
    List<Society> findSocietiesByUniversityID(String universityId);
}
