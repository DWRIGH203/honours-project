package graphQL.dataLoader;

import graphQL.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

@Component
public class MyDataLoader {
    private final StaffRepository staffRepository;
    private final StudentRepository studentRepository;
    private final CourseRepository courseRepository;
    private final SocietyRepository societyRepository;

    @Autowired
    MyDataLoader(StaffRepository staffRepository, StudentRepository studentRepository,
                 CourseRepository courseRepository, SocietyRepository societyRepository) {
        this.staffRepository = staffRepository;
        this.studentRepository = studentRepository;
        this.courseRepository = courseRepository;
        this.societyRepository = societyRepository;
    }

        @PostConstruct
    private void generateData() {

        staffRepository.deleteAll();
        studentRepository.deleteAll();
        courseRepository.deleteAll();
        societyRepository.deleteAll();


        // ------------------ Staff Data -------------------------------

            StaffDataLoader staffDataLoader = new StaffDataLoader(staffRepository);

            int staffCounter = 0;
            int j = 5;

            while(staffCounter < j) {
                staffDataLoader.addStaff("123");
                staffDataLoader.addStaff("345");
                staffDataLoader.addStaff("567");
                staffCounter++;
            }


        // -------------------- Society Data ------------------------------

            SocietyDataLoader societyDataLoader = new SocietyDataLoader(societyRepository);

            int societyCounter = 0;
            int k = 5;

            while(societyCounter < k) {
              societyDataLoader.addSocieties("345");
              societyDataLoader.addSocieties("123");
              societyDataLoader.addSocieties("567");
              societyCounter++;
            }


        //------------------ Student Data ------------------------------------------

            StudentDataLoader studentDataLoader = new StudentDataLoader(studentRepository);

            int studentCounter = 0;
            int x = 24;

            while(studentCounter < x) {
                studentDataLoader.addStudents("123");
                studentDataLoader.addStudents("345");
                studentDataLoader.addStudents("567");
                studentCounter++;
            }

            //------------------ COURSE DATA -------------------------

            CourseDataLoader courseDataLoader = new CourseDataLoader(courseRepository);

            int courseCounter = 0;
            int y = 15;

            while(courseCounter < y) {
                courseDataLoader.addCourses("123");
                courseDataLoader.addCourses("345");
                courseDataLoader.addCourses("567");
                courseCounter++;
            }
        }
}
