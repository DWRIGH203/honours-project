package graphQL.graphql_utilities;

import graphQL.dataFetchers.*;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;

import static graphql.GraphQL.newGraphQL;
import static graphql.schema.idl.RuntimeWiring.newRuntimeWiring;

@Component
public class GraphQlUtility {

    @Value("classpath:schemas.graphqls")
    private Resource schemaResource;
    private AllUniversityDataFetcher allUniversityDataFetcher;
    private UniversityByIdDataFetcher universityByIdDataFetcher;

    @Autowired
    GraphQlUtility(AllUniversityDataFetcher allUniversityDataFetcher,
                   UniversityByIdDataFetcher universityByIdDataFetcher) throws IOException {
        this.allUniversityDataFetcher = allUniversityDataFetcher;
        this.universityByIdDataFetcher = universityByIdDataFetcher;


    }

    @PostConstruct
    public GraphQL createGraphQlObject() throws IOException {
        File schemas = schemaResource.getFile();
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(schemas);
        RuntimeWiring wiring = buildRuntimeWiring();
        GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeRegistry, wiring);
        return  newGraphQL(schema).build();
    }

    private RuntimeWiring buildRuntimeWiring(){
        return newRuntimeWiring()
                .type("Query", typeWiring -> typeWiring
                    .dataFetcher("allUniversitiesQuery", allUniversityDataFetcher)
                    .dataFetcher("universityByIdQuery", universityByIdDataFetcher)
                )
                .build();
    }
}

