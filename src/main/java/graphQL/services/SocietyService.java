package graphQL.services;

import graphQL.models.Society;
import java.util.List;

public interface SocietyService {

    List<Society> getAllSocieties();
    List<Society> getSocietiesByUniversityID(String universityID);
}
