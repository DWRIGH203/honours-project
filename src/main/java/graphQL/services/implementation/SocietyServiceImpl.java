package graphQL.services.implementation;

import graphQL.models.Society;
import graphQL.repositories.SocietyRepository;
import graphQL.services.SocietyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SocietyServiceImpl implements SocietyService {

    @Autowired
    SocietyRepository societyRepository;

    @Override
    public List<Society> getAllSocieties() {
        return (List<Society>) societyRepository.findAll();
    }

    @Override
    public List<Society> getSocietiesByUniversityID(String universityID) {
        return societyRepository.findSocietiesByUniversityID(universityID);
    }
}
