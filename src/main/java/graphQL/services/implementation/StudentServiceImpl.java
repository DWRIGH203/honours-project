package graphQL.services.implementation;

import graphQL.models.Student;
import graphQL.repositories.StudentRepository;
import graphQL.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Override
    public List<Student> getAllStudents() {
        return (List<Student>) studentRepository.findAll();
    }

    @Override
    public List<Student> getStudentByUniversityId(String universityID) {
        return studentRepository.findStudentsByUniversityId(universityID);
    }
}
