package graphQL.services.implementation;

import graphQL.models.Staff;
import graphQL.repositories.StaffRepository;
import graphQL.services.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StaffServiceImpl implements StaffService {

    @Autowired
    StaffRepository staffRepository;

    @Override
    public List<Staff> getAllStaff() {
        return (List<Staff>) staffRepository.findAll();
    }

    @Override
    public List<Staff> getStaffByUniversityId(String universityID) {
        return staffRepository.findStaffByUniversityId(universityID);
    }
}
