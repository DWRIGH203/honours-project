package graphQL.services.implementation;

import graphQL.models.Course;
import graphQL.repositories.CourseRepository;
import graphQL.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    CourseRepository courseRepository;

    @Override
    public List<Course> getAllCourses() {
        return (List<Course>) courseRepository.findAll();
    }

    @Override
    public List<Course> getCoursesByUniversityId(String universityID) {
        return courseRepository.findCoursesByUniversityId(universityID);
    }
}
