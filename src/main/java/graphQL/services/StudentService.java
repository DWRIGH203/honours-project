package graphQL.services;

import graphQL.models.Student;
import java.util.List;

public interface StudentService {

    List<Student> getAllStudents();
    List<Student> getStudentByUniversityId(String universityID);

}
