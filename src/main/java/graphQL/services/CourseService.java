package graphQL.services;

import graphQL.models.Course;
import java.util.List;

public interface CourseService {

    List<Course> getAllCourses();
    List<Course> getCoursesByUniversityId(String universityID);
}
