package graphQL.services;

import graphQL.models.Staff;
import java.util.List;

public interface StaffService {

    List<Staff> getAllStaff();
    List<Staff> getStaffByUniversityId(String universityID);

}
