package graphQL.controllers;

import graphQL.models.*;
import graphQL.services.*;
import graphql.ExecutionResult;
import graphql.GraphQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import graphQL.graphql_utilities.GraphQlUtility;

import java.io.IOException;
import java.util.List;

@RestController
public class MainController {

    private GraphQL graphQL;

    @Autowired
    private StaffService staffService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private SocietyService societyService;

    @Autowired
    private CourseService courseService;

    @Autowired
    MainController(GraphQlUtility graphQlUtility) throws IOException {
        graphQL = graphQlUtility.createGraphQlObject();
    }

    //GraphQL Single Request Mapping
    @PostMapping(value = "/graphql")
    public ResponseEntity query(@RequestBody String query){
        ExecutionResult result = graphQL.execute(query);
        return ResponseEntity.ok(result.getData());
    }

    //    Scenario A - Get society and staff name & email by University ID
    @GetMapping(value = "/rest/societies/{universityID}")
    public List<Society> getSocietiesByUniversity(@PathVariable String universityID) {
        return societyService.getSocietiesByUniversityID(universityID);
    }


    //Scenario A & B Get Staff Data
    @GetMapping(value = "/rest/staff/{universityID}")
    public List<Staff> getStaffByUniversity(@PathVariable String universityID) {
        return staffService.getStaffByUniversityId(universityID);
    }


    //    Scenario B - Get all student names and exam results
    @GetMapping(value = "/rest/students/{universityID}")
    public List<Student> getStudentsByUniversity(@PathVariable String universityID) {
        return studentService.getStudentByUniversityId(universityID);
    }

    //Scenario B - Get All Course Data

    @GetMapping(value = "/rest/courses/{universityID}")
    public List<Course> getCoursesByUniversity(@PathVariable String universityID) {
        return courseService.getCoursesByUniversityId(universityID);
    }

    //Scenario C - Get All Courses, Students, Society, Staff

    @GetMapping(value = "/rest/courses")
    public List<Course> getAllCourses() {
        return courseService.getAllCourses();
    }

    @GetMapping(value = "/rest/students")
    public List<Student> getAllStudents() {
        return studentService.getAllStudents();
    }

    @GetMapping(value = "/rest/societies")
    public List<Society> getAllSocieties() {
        return societyService.getAllSocieties();
    }

    @GetMapping(value = "/rest/staff")
    public List<Staff> getAllStaff() {
        return staffService.getAllStaff();
    }


}
