package graphQL.dataFetchers;

import graphQL.models.*;
import graphQL.services.CourseService;
import graphQL.services.SocietyService;
import graphQL.services.StaffService;
import graphQL.services.StudentService;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class UniversityByIdDataFetcher implements DataFetcher<University> {

    private StaffService staffService;
    private StudentService studentService;
    private CourseService courseService;
    private SocietyService societyService;

    @Autowired
    UniversityByIdDataFetcher(StaffService staffService, StudentService studentService, CourseService courseService,
                              SocietyService societyService){
        this.staffService = staffService;
        this.studentService = studentService;
        this.courseService = courseService;
        this.societyService = societyService;
    }

    @Override
    public University get(DataFetchingEnvironment env) {
        Map args = env.getArguments();

        University university = new University();
        List<Staff> staff = staffService.getStaffByUniversityId((String.valueOf(args.get("universityID"))));
        List<Student> students = studentService.getStudentByUniversityId((String.valueOf(args.get("universityID"))));
        List<Course> courses = courseService.getCoursesByUniversityId((String.valueOf(args.get("universityID"))));
        List<Society> societies = societyService.getSocietiesByUniversityID((String.valueOf(args.get("universityID"))));

        university.setStaff(staff);
        university.setStudents(students);
        university.setCourses(courses);
        university.setSocieties(societies);

        return university;

    }
}
