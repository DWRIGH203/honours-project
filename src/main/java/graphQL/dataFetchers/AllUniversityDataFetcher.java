package graphQL.dataFetchers;

import graphQL.models.University;
import graphQL.services.CourseService;
import graphQL.services.SocietyService;
import graphQL.services.StaffService;
import graphQL.services.StudentService;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component

public class AllUniversityDataFetcher implements DataFetcher<University> {

    private StaffService staffService;
    private StudentService studentService;
    private CourseService courseService;
    private SocietyService societyService;

    @Autowired
    AllUniversityDataFetcher(StaffService staffService, StudentService studentService, CourseService courseService,
                             SocietyService societyService){

        this.staffService = staffService;
        this.studentService = studentService;
        this.courseService = courseService;
        this.societyService = societyService;
    }


    @Override
    public University get(DataFetchingEnvironment dataFetchingEnvironment) {

        University university = new University();
        university.setStaff(staffService.getAllStaff());
        university.setStudents(studentService.getAllStudents());
        university.setCourses(courseService.getAllCourses());
        university.setSocieties(societyService.getAllSocieties());
        return university;
    }
}
